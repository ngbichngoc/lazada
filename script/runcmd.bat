REM Script to run automation test for Windows
REM Flag for Jenkin
SET "ERROR_FLAG=1"

REM PRECONDITION: SET CLASSPATH IN WINDOWS SYSTEM
SET SELENIUM_HOME="%CD%\lib\SELENIUM_HOME"
SET TESTNG_HOME="%CD%\lib\TESTNG_HOME"
SET POI_HOME="%CD%\lib\POI_HOME"

REM SET CLASSPATH 
SET "CLASSPATH=%CLASSPATH%;.\..\src;%SELENIUM_HOME%\*;%SELENIUM_HOME%\libs\*;%TESTNG_HOME%\*;%POI_HOME%\lib\*;%POI_HOME%\*;"

REM Set Javac path
SET "PATH=%PATH%;C:\Program Files\Java\jdk1.8.0_40\bin;"

REM set workspace directory
REM SET "WORKSPACE=.\.."
SET "WORKSPACE=.\.."

REM Delete files which created by previous job 
RMDIR /s /q %WORKSPACE%\bin
MKDIR %WORKSPACE%\bin

MKDIR %WORKSPACE%\result\old
MOVE /Y %WORKSPACE%\result\*.xls %WORKSPACE%\result\old\

CD %WORKSPACE%\src

REM BUILD
FOR /R %%a IN (*.java) DO ( javac -encoding ISO-8859-1 -classpath %CLASSPATH% -d %WORKSPACE%\bin "%%a" ) || SET "ERROR_FLAG=1"

REM CD ..

REM RUN TEST
JAVA -cp "%CLASSPATH%;bin" org.testng.TestNG conf\testng.xml -d result\test-output\

PAUSE 

EXIT %ERROR_FLAG%
