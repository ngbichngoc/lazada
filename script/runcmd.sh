#########################################
## Sample for Auto Test Script
#########################################
## Use this file in the root directory for test
#!/bin/bash

# Set WORKSPACE 
export WORKSPACE=$PWD

# Set CLASSPATH
export CLASSPATH=$CLASSPATH:$WORKSPACE/src:$WORKSPACE/lib/SELENIUM_HOME/*:$WORKSPACE/lib/SELENIUM_HOME/libs/*:$WORKSPACE/lib/TESTNG_HOME/*:$WORKSPACE/lib/POI_HOME/*:$WORKSPACE/lib/POI_HOME/lib/*

# Clear bin directory
mkdir -p $WORKSPACE/bin
rm -rf $WORKSPACE/bin/*

# Remove old result to old folder
mkdir -p $WORKSPACE/result/old

# if [ -f $WORKSPACE/result/*.xls ]; then
  mv -f $WORKSPACE/result/*.xls $WORKSPACE/result/old/
# fi

# Build 
#### Only build the needed source code
#### This is sample
cd $WORKSPACE

find $WORKSPACE/src -name *.java -type f | while read FILE
do
    javac -encoding UTF-8 -d $WORKSPACE/bin ${FILE} -Xlint:deprecation -Xlint:unchecked
done

# Run test
java -cp "${CLASSPATH}:$WORKSPACE/bin" org.testng.TestNG $WORKSPACE/conf/testng.xml -d $WORKSPACE/result/test-output/