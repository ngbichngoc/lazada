package testSuite;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import testSuite.Initialize;


public class ProductInformation extends Initialize {

	/**
	 * [[SPEC]]
	 * [[ID]]
	 * 009
	 * [[TITLE]]
	 * Validate product informations in home page
	 * [[CHECKPOINT]]
	 * Product information between product detail page and cart page are the same
	 * [[HOWTO]]
	 * 1. Select a product in home page
	 * 2. Press on Add to cart button
	 * 3. Check the product information between detail page and product page
	 * [[SUCCESS]]
	 * 1. Detail product page is opened
	 * 2. Cart page is opened
	 * 3. Product information between product detail page and cart page are the same
	 * [[ENVIRONMENT]]
	 * 1. Access the URL www.lazada.vn
	 * [[NOTE]]
	 * 
	 * [[PRIORITY]]
	 * 
	 * [[TAG]]
	 * 
	 * [[FILE]]
	 * 
	 * [[BUG]]
	 * 
	 * [[ENDSPEC]]
	 **/
	@Test(enabled = true, groups = { "CI" })
	public void Testcase_009() throws Exception  {
		String title = null;
		String specialPrice = null;
		// [ENVIRONMENT]
		// 1. Access the URL www.lazada.vn
		
		// [HOWTO]
		// 1. Select a product in home page
		driver.findElement(By.id("countdownLongbar")).click();
		WaitForElementEnable(By.xpath("/html/body/div[1]/div[2]/div[2]/div/div[1]/div[1]/div/a/div"), 20);
		
		driver.findElement(By.xpath("//*[@id=\"div_product_detail\"]/div[1]/div[2]/div/p[2]/a")).click();
		WaitForElementEnable(By.xpath("//*[@id=\"prodinfo\"]/div[1]/div/div[1]/div"), 20);
		// 1. Detail product page is opened
		title = driver.findElement(By.id("prod_title")).getText().trim();
		specialPrice = driver.findElement(By.id("special_price_box")).getText().trim();
		
		// 2. Press on Add to cart button
		if (isElementPresent(By.xpath("//*[@id=\"OptionsMultiDropdown\"]/span/select"))) {
			new Select(driver.findElement(By.xpath("//*[@id=\"OptionsMultiDropdown\"]/span/select"))).selectByIndex(1);
		}
		driver.findElement(By.xpath("//*[@id=\"AddToCart\"]/span[1]")).click();
		WaitForElementEnable(By.id("three_step_checkout"), 20);
		// 2. Cart page is opened
		
		// 3. Check the product information between detail page and product page
		// 3. Product information between product detail page and cart page are the same
		AssertEqualByXpath("//*[@id=\"cart-items-list-form\"]/div[2]/table/tbody/tr/td[2]/div[1]", title);
		AssertEqualByXpath("//*[@id=\"cart-items-list-form\"]/div[2]/table/tbody/tr/td[3]", specialPrice);

		Thread.sleep(3000);
	}


}
