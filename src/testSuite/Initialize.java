package testSuite;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Dimension;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;

import utility.CommonClass;
import utility.ExcelUtils;
import utility.Locale;
import utility.LocaleENG;
import utility.LocaleVI;
import utility.ParseUtils;
import utility.TestResult;
import utility.Utilities;

public class Initialize extends Utilities {
	public WebDriver driver;
	public Actions action;
	public WebDriverWait waiting;
	public Locale ms = new Locale();
	/*
	 * reset = 0: Test case is OK and chrome driver does not need to reset
	 * reset = 1: Test case is NG and chrome driver needs to reset
	 * reset = 2: Test case is OK but chrome driver needs to reset
	 */
	public int reset = 1;
	/*
	 * index: start row in test result files
	 */
	public int index = 2;
	// Wait time for elements
	public int driver_wait_time;

	private static String testUrl;
	private static String testEnv;
	private static String testLocale;
	private static String folderResult = "";
	private static String folderResultTemp = "";
	private static String fileTestResult = "";
	public static String URL;
	private String sheetname = "Sheet 1";
	private String timestamp = "";
	private List<TestResult> resultList = new ArrayList<TestResult>();
	private TestResult r;
	private ParseUtils parse;
	private ExcelUtils excel;

	private String testID;
	private String testResult;
	private static String testVersion = null;
	private String testReport;
	private String testCategory;

	/**
	 * Create temp folder for test result
	 */
	@BeforeSuite(alwaysRun = true)
	@Parameters(value = { "confFile", "test_env", "test_url", "test_locale", "folderResult",
			"folderTemp", "test_category" })
	public void setUp(String sel_confFile, String sel_env, String sel_url, String sel_locale, String sel_folderResult, String sel_folderTemp,
			String sel_category)
			throws Exception {
		// Get timestamp
		timestamp = timestamp();
		
		// Get config value
		folderResult = sel_folderResult;
		folderResultTemp = sel_folderTemp;
		fileTestResult = "LAZADA_TestResult_" + sel_category;
		testUrl = sel_url;
		testEnv = sel_env;
		testLocale = sel_locale;
		testCategory = sel_confFile;
		
		// Delete temp folder if exist
		removeDir(folderResultTemp);
		// Make temp folder
		makeDir(folderResultTemp);

		// Get all run source code in config file
		parse = new ParseUtils(testCategory);
	}

	/**
	 * Create test result file for each class
	 **/
	@BeforeClass(alwaysRun = true)
	public void createTestResult(ITestContext context) throws Exception {
		// Create test result temp file
		excel = new ExcelUtils(
				folderResultTemp + "CloudTV_TestResult_" + timestamp(), sheetname);
		excel.createTestResultTempFile();
	}

	/**
	 * Set the testing properties before each test class.
	 **/
	@BeforeMethod(alwaysRun = true)
	public void launchBrowser(Method method) throws Exception {	
		// Get test ID
		testID = method.getName().replace("Testcase_", "");
		// Printout test ID (During Debugging It is helpful TEST ID is displayed when test starts (by UKAI)
		System.out.println("==== Testcase ID: Testcase " + testID + " Starting (" + timestamp() + ")=====");

		// Check flag and index
		if (reset == 1) {
			// Set language
			InitLanguage(testLocale);

//			// Set URL with locale language
			if (!testLocale.equals(""))
				URL = testUrl + "?setLang=" + testLocale;

			if (testEnv.equals("wins"))
				System.setProperty("webdriver.chrome.driver",
						CommonClass.pathDriverWins);
			else if (testEnv.equals("linux32"))
				System.setProperty("webdriver.chrome.driver",
						CommonClass.pathDriverLinux32bit);
			else
				System.setProperty("webdriver.chrome.driver",
						CommonClass.pathDriverLinux64bit);
			// Add fullscreen mode
			driver = new ChromeDriver();
			
			// Init driver
			InitialDriver(driver, URL);		
		
		}
	}

	/**
	 * After each test case, check the result
	 **/
	@AfterMethod(alwaysRun = true)
	public void stopDriver(ITestResult tr) throws Exception {
		// Get test result
		testResult = getTestResult(tr.getStatus());
		// Get test throwable
		parse = new ParseUtils();
		testReport = parse
				.traceError(this.getClass().getName(), tr.getThrowable());

		// If test result is NG, quit driver, and re-init at next TCs
		if (testResult == "OK" && reset == 2) {
			reset = 1;
			driver.quit();
		} else if (testResult == "NG") {
			reset = 1;
			driver.quit();
		} else if (testResult == "OK") {
			// takeScreenShot(tr.getName());
			reset = 0;
		} else {
			reset = 0;
		}
		
		// Add result
		r = new TestResult(testID, testResult, testVersion, testReport);
		resultList.add(r);
	}

	/**
	 * Add test result for each class
	 */
	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		// Write test result
		excel.writeTestResultToFile(0, this.getClass().getName(), resultList);
		// Quit driver
		driver.quit();
	}

	/**
	 * Create test result
	 */
	@AfterSuite(alwaysRun = true)
	public void mergeTestResult() throws Exception {
		excel = new ExcelUtils(folderResultTemp + "ATestResult", sheetname);
		excel.createTestResultFile();
		// Check all test result file in temp folder
		List<FileInputStream> listFile = new ArrayList<FileInputStream>();
		listFile = getFilesInDir(folderResultTemp);
		
		// Create test result file name with timestamp																																																																																			
		String filename = folderResult + fileTestResult + "_" + timestamp + ".xls";
		// Merger test result
		excel.mergeExcelFiles(new File(filename), listFile);
		// Remove temp file
		removeDir(folderResultTemp);
	}


	/**
	 * Convert test result in TestNG from int to String
	 * 
	 * @param result
	 *            get in TestNG
	 * @return
	 */
	private String getTestResult(int result) {
		String testResult = "";
		switch (result) {
		case ITestResult.SUCCESS:
			testResult = "OK";
			break;
		case ITestResult.FAILURE:
			testResult = "NG";
			break;
		case ITestResult.SKIP:
			testResult = "NA";
			break;
		default:
			testResult = "";
			break;
		}
		return testResult;
	}

	/**
	 * Only init driver
	 * 
	 * @param dr
	 * @throws InterruptedException 
	 */
	protected void InitialDriver(WebDriver dr, String strUrl) throws InterruptedException {
		driver_wait_time = 60;

		dr.manage().deleteAllCookies();
		dr.manage().timeouts()
				.implicitlyWait(driver_wait_time, TimeUnit.SECONDS);
		dr.manage().timeouts()
				.pageLoadTimeout(driver_wait_time, TimeUnit.SECONDS);
		// fit to cloud window size
		dr.manage().window().setSize(new Dimension(1280, 850));
				
		action = new Actions(dr);

		dr.get(strUrl);		
		
		waiting = new WebDriverWait(dr, driver_wait_time); // wait 30 seconds for initial launch 		
		waiting.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("/html/body/div[1]/div[1]/div/ul/li[5]/span")));
		Thread.sleep(3000);
	}	
	



	/**
	 * Assert message on screen by xpath
	 * 
	 * @param xPath
	 * @param errMs
	 */
	public void AssertEqualByXpath(String xPath, String errMs) {
		Assert.assertEquals(driver.findElement(By.xpath(xPath)).getText(),
				errMs);
	}

	/**
	 * Assert message on screen by xpath
	 * 
	 * @param id
	 * @param errMs
	 */
	public void AssertEqualByID(String id, String errMs) {
		Assert.assertEquals(driver.findElement(By.id(id)).getText(), errMs);
	}

	/**
	 * Change language, depend on local settings
	 * 
	 * @param locale
	 * @throws Exception
	 */
	public void InitLanguage(String locale) throws Exception {
		/*
		 * Init language
		 */
		switch (locale) {
		case "en":
			// English
			ms = new LocaleENG();
			break;
		case "vi":
			// Simplified Chinese
			ms = new LocaleVI();
			break;
		default:
			ms = new LocaleENG();
			break;
		}
	}

	
	/**
	 * Check element presents or not by xpath
	 * @param xpath
	 * @return
	 */
	public boolean isElementPresent(By locatorKey) {
		boolean result;
		// Reduce wait time to 1s
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
	    try {
	        driver.findElement(locatorKey);
	        result = true;
	    } catch (org.openqa.selenium.NoSuchElementException e) {
	        result = false;
	    }
	    // Reset wait time
	    driver.manage().timeouts().implicitlyWait(driver_wait_time, TimeUnit.SECONDS);
	    return result;
	}
	
	/**
	 * Wait for an element disable
	 * @param locatorKey
	 * @param loop
	 * @throws Exception
	 */
	public void WaitForElementDisable(By locatorKey, int loop) throws Exception {
		int loop_count = 0;
		while (isElementPresent(locatorKey)) {
			Thread.sleep(500);
			loop_count++;
		    if(loop_count > loop){
		    	Assert.fail("Timeout for element disable");
		    }
		}
	}
	
	/**
	 * Wait for an element enable
	 * @param locatorKey
	 * @param loop
	 * @throws Exception
	 */
	public void WaitForElementEnable(By locatorKey, int loop) throws Exception {
		int loop_count = 0;
		while (!isElementPresent(locatorKey)) {
			Thread.sleep(500);
			loop_count++;
		    if(loop_count > loop){
		    	Assert.fail("Timeout for element enable");
		    }
		}
	}
	
	

}
