package testSuite;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import testSuite.Initialize;


public class RegisterAccount extends Initialize {

	/**
	 * [[SPEC]]
	 * [[ID]]
	 * 001
	 * [[TITLE]]
	 * Validate error message when registering a new account with username is empty
	 * [[CHECKPOINT]]
	 * Error message
	 * [[HOWTO]]
	 * 1. Press on [Register] button on the top right
	 * 2. Input register information except username
	 * 3. Press on [Send] button
	 * [[SUCCESS]]
	 * 1. Register menu is opened
	 * 2. Information is inputted
	 * 3. Error message is displayed:
	 * - "Thông tin bắt buộc"
	 * [[ENVIRONMENT]]
	 * 1. Access the URL www.lazada.vn
	 * [[NOTE]]
	 * 
	 * [[PRIORITY]]
	 * 
	 * [[TAG]]
	 * 
	 * [[FILE]]
	 * 
	 * [[BUG]]
	 * 
	 * [[ENDSPEC]]
	 **/
	@Test(enabled = true, groups = { "CI" })
	public void Testcase_001() throws Exception  {
		// [ENVIRONMENT]
		// 1. Access the URL www.lazada.vn

		// [HOWTO]
		// 1. Press on [Register] button on the top right
		OpenSignUpMenu();
		// 1. Register menu is opened
		
		// 2. Input register information except username
		driver.findElement(By.id("RegistrationForm_first_name")).clear();
		driver.findElement(By.id("RegistrationForm_email")).sendKeys("myemail@gmail.com");
		driver.findElement(By.id("RegistrationForm_password")).sendKeys("12345678");
		driver.findElement(By.id("RegistrationForm_password2")).sendKeys("12345678");
		// 2. Information is inputted
		
		// 3. Press on [Send] button
		driver.findElement(By.id("send")).click();
		// 3. Error message is displayed:
		// - "Required field"
		AssertEqualByXpath("//*[@id=\"popup-form-account-create\"]/fieldset/div/div[1]/div[2]/div/span", ms.userEmptyError);
		Thread.sleep(3000);
	}
	
	/**
	 * [[SPEC]]
	 * [[ID]]
	 * 002
	 * [[TITLE]]
	 * Validate error message when registering a new account with email is empty
	 * [[CHECKPOINT]]
	 * Error message
	 * [[HOWTO]]
	 * 1. Press on [Register] button on the top right
	 * 2. Input register information except email
	 * 3. Press on [Send] button
	 * [[SUCCESS]]
	 * 1. Register menu is opened
	 * 2. Information is inputted
	 * 3. Error message is displayed:
	 * - "Required field"
	 * [[ENVIRONMENT]]
	 * 1. Access the URL www.lazada.vn
	 * [[NOTE]]
	 * 
	 * [[PRIORITY]]
	 * 
	 * [[TAG]]
	 * 
	 * [[FILE]]
	 * 
	 * [[BUG]]
	 * 
	 * [[ENDSPEC]]
	 **/
	@Test(enabled = true, groups = { "CI" })
	public void Testcase_002() throws Exception  {
		// [ENVIRONMENT]
		// 1. Access the URL www.lazada.vn

		// [HOWTO]
		// 1. Press on [Register] button on the top right
		OpenSignUpMenu();
		// 1. Register menu is opened
		
		// 2. Input register information except email
		driver.findElement(By.id("RegistrationForm_first_name")).sendKeys("My name");
		driver.findElement(By.id("RegistrationForm_email")).clear();
		driver.findElement(By.id("RegistrationForm_password")).sendKeys("12345678");
		driver.findElement(By.id("RegistrationForm_password2")).sendKeys("12345678");
		// 2. Information is inputted
		
		// 3. Press on [Send] button
		driver.findElement(By.id("send")).click();
		// 3. Error message is displayed:
		// - "Required field"
		AssertEqualByXpath("//*[@id=\"popup-form-account-create\"]/fieldset/div/div[2]/div[2]/div/span", ms.emailEmptyError);
		
		Thread.sleep(3000);
	}


	/**
	 * [[SPEC]]
	 * [[ID]]
	 * 003
	 * [[TITLE]]
	 * Validate error message when registering a new account with email is invalid format
	 * [[CHECKPOINT]]
	 * Error message
	 * [[HOWTO]]
	 * 1. Press on [Register] button on the top right
	 * 2. Input register information but email is invalid format (not aaa@aaa.aaa)
	 * 3. Press on [Send] button
	 * [[SUCCESS]]
	 * 1. Register menu is opened
	 * 2. Information is inputted
	 * 3. Error message is displayed:
	 * - "Invalid mail"
	 * [[ENVIRONMENT]]
	 * 1. Access the URL www.lazada.vn
	 * [[NOTE]]
	 * 
	 * [[PRIORITY]]
	 * 
	 * [[TAG]]
	 * 
	 * [[FILE]]
	 * 
	 * [[BUG]]
	 * 
	 * [[ENDSPEC]]
	 **/
	@Test(enabled = true, groups = { "CI" })
	public void Testcase_003() throws Exception  {
		// [ENVIRONMENT]
		// 1. Access the URL www.lazada.vn


		// [HOWTO]
		// 1. Press on [Register] button on the top right
		OpenSignUpMenu();
		// 1. Register menu is opened
		
		// 2. Input register information but email is invalid format (not aaa@aaa.aaa)
		driver.findElement(By.id("RegistrationForm_first_name")).sendKeys("My name");
		driver.findElement(By.id("RegistrationForm_email")).sendKeys("Invalid email");
		driver.findElement(By.id("RegistrationForm_password")).sendKeys("12345678");
		driver.findElement(By.id("RegistrationForm_password2")).sendKeys("12345678");
		// 2. Information is inputted
		
		// 3. Press on [Send] button
		driver.findElement(By.id("send")).click();
		// 3. Error message is displayed:
		// - "Invalid mail"
		AssertEqualByXpath("//*[@id=\"popup-form-account-create\"]/fieldset/div/div[2]/div[2]/div/span", ms.emailInvalidError);
		
		Thread.sleep(3000);
	}
	
	/**
	 * [[SPEC]]
	 * [[ID]]
	 * 004
	 * [[TITLE]]
	 * Validate error message when registering a new account with password is empty
	 * [[CHECKPOINT]]
	 * Error message
	 * [[HOWTO]]
	 * 1. Press on [Register] button on the top right
	 * 2. Input register information except password
	 * 3. Press on [Send] button
	 * [[SUCCESS]]
	 * 1. Register menu is opened
	 * 2. Information is inputted
	 * 3. Error message is displayed:
	 * - "Password must contain at least one number"
	 * [[ENVIRONMENT]]
	 * 1. Access the URL www.lazada.vn
	 * [[NOTE]]
	 * 
	 * [[PRIORITY]]
	 * 
	 * [[TAG]]
	 * 
	 * [[FILE]]
	 * 
	 * [[BUG]]
	 * 
	 * [[ENDSPEC]]
	 **/
	@Test(enabled = true, groups = { "CI" })
	public void Testcase_004() throws Exception  {
		// [ENVIRONMENT]
		// 1. Access the URL www.lazada.vn

		// [HOWTO]
		// 1. Press on [Register] button on the top right
		OpenSignUpMenu();
		// 1. Register menu is opened
		
		// 2. Input register information except password
		driver.findElement(By.id("RegistrationForm_first_name")).sendKeys("My name");
		driver.findElement(By.id("RegistrationForm_email")).sendKeys("myemail@gmail.com");
		driver.findElement(By.id("RegistrationForm_password")).clear();
		driver.findElement(By.id("RegistrationForm_password2")).sendKeys("12345678");
		// 2. Information is inputted
		
		// 3. Press on [Send] button
		driver.findElement(By.id("send")).click();
		// 3. Error message is displayed:
		// - "Password must contain at least one number"
		AssertEqualByXpath("//*[@id=\"popup-form-account-create\"]/fieldset/div/div[3]/div[2]/div/span", ms.pwEmptyError);
		
		Thread.sleep(3000);
	}


	/**
	 * [[SPEC]]
	 * [[ID]]
	 * 005
	 * [[TITLE]]
	 * Validate error message when registering a new account with password is less than 6 characters
	 * [[CHECKPOINT]]
	 * Error message
	 * [[HOWTO]]
	 * 1. Press on [Register] button on the top right
	 * 2. Input register information but password is less than 6 characters
	 * 3. Press on [Send] button
	 * [[SUCCESS]]
	 * 1. Register menu is opened
	 * 2. Information is inputted
	 * 3. Error message is displayed:
	 * - "Password is too short (minimum is 6 characters)."
	 * [[ENVIRONMENT]]
	 * 1. Access the URL www.lazada.vn
	 * [[NOTE]]
	 * 
	 * [[PRIORITY]]
	 * 
	 * [[TAG]]
	 * 
	 * [[FILE]]
	 * 
	 * [[BUG]]
	 * 
	 * [[ENDSPEC]]
	 **/
	@Test(enabled = true, groups = { "CI" })
	public void Testcase_005() throws Exception  {

		// [ENVIRONMENT]
		// 1. Access the URL www.lazada.vn

		// [HOWTO]
		// 1. Press on [Register] button on the top right
		OpenSignUpMenu();
		// 1. Register menu is opened
		
		// 2. Input register information but password is less than 6 characters
		driver.findElement(By.id("RegistrationForm_first_name")).sendKeys("My name");
		driver.findElement(By.id("RegistrationForm_email")).sendKeys("myemail@gmail.com");
		driver.findElement(By.id("RegistrationForm_password")).sendKeys("12abc");
		driver.findElement(By.id("RegistrationForm_password2")).sendKeys("12abc");
		// 2. Information is inputted
		
		// 3. Press on [Send] button
		driver.findElement(By.id("send")).click();
		// 3. Error message is displayed:
		// - "Password is too short (minimum is 6 characters)."
		AssertEqualByXpath("//*[@id=\"popup-form-account-create\"]/fieldset/div/div[3]/div[2]/div/span", ms.pwShortError);
		
		Thread.sleep(3000);

	}


	/**
	 * [[SPEC]]
	 * [[ID]]
	 * 006
	 * [[TITLE]]
	 * Validate error message when registering a new account with password has no alphabet characters
	 * [[CHECKPOINT]]
	 * Error message
	 * [[HOWTO]]
	 * 1. Press on [Register] button on the top right
	 * 2. Input register information but password has no alphabet character
	 * 3. Press on [Send] button
	 * [[SUCCESS]]
	 * 1. Register menu is opened
	 * 2. Information is inputted
	 * 3. Error message is displayed:
	 * - "Password must contain at least one alphanumeric"
	 * [[ENVIRONMENT]]
	 * 1. Access the URL www.lazada.vn
	 * [[NOTE]]
	 * 
	 * [[PRIORITY]]
	 * 
	 * [[TAG]]
	 * 
	 * [[FILE]]
	 * 
	 * [[BUG]]
	 * 
	 * [[ENDSPEC]]
	 **/
	@Test(enabled = true, groups = { "CI" })
	public void Testcase_006() throws Exception  {

		// [ENVIRONMENT]
		// 1. Access the URL www.lazada.vn

		// [HOWTO]
		// 1. Press on [Register] button on the top right
		OpenSignUpMenu();
		// 1. Register menu is opened

		// 2. Input register information but password has no alphabet character
		driver.findElement(By.id("RegistrationForm_first_name")).sendKeys("My name");
		driver.findElement(By.id("RegistrationForm_email")).sendKeys("myemail@gmail.com");
		driver.findElement(By.id("RegistrationForm_password")).sendKeys("123456");
		driver.findElement(By.id("RegistrationForm_password2")).sendKeys("123456");
		// 2. Information is inputted

		// 3. Press on [Send] button
		driver.findElement(By.id("send")).click();
		// 3. Error message is displayed:
		// - "Password must contain at least one alphanumeric"
		AssertEqualByXpath("//*[@id=\"popup-form-account-create\"]/fieldset/div/div[3]/div[2]/div/span",
				ms.pwNoAlphabeError);

		Thread.sleep(3000);

	}


	/**
	 * [[SPEC]]
	 * [[ID]]
	 * 007
	 * [[TITLE]]
	 * Validate error message when registering a new account with password has no numeric characters
	 * [[CHECKPOINT]]
	 * Error message
	 * [[HOWTO]]
	 * 1. Press on [Register] button on the top right
	 * 2. Input register information but password has no numeric character
	 * 3. Press on [Send] button
	 * [[SUCCESS]]
	 * 1. Register menu is opened
	 * 2. Information is inputted
	 * 3. Error message is displayed:
	 * - "Password must contain at least one number"
	 * [[ENVIRONMENT]]
	 * 1. Access the URL www.lazada.vn
	 * [[NOTE]]
	 * 
	 * [[PRIORITY]]
	 * 
	 * [[TAG]]
	 * 
	 * [[FILE]]
	 * 
	 * [[BUG]]
	 * 
	 * [[ENDSPEC]]
	 **/
	@Test(enabled = true, groups = { "CI" })
	public void Testcase_007() throws Exception  {

		// [ENVIRONMENT]
		// 1. Access the URL www.lazada.vn

		// [HOWTO]
		// 1. Press on [Register] button on the top right
		OpenSignUpMenu();
		// 1. Register menu is opened

		// 2. Input register information but password has no numeric character
		driver.findElement(By.id("RegistrationForm_first_name")).sendKeys("My name");
		driver.findElement(By.id("RegistrationForm_email")).sendKeys("myemail@gmail.com");
		driver.findElement(By.id("RegistrationForm_password")).sendKeys("abcdef");
		driver.findElement(By.id("RegistrationForm_password2")).sendKeys("abcdef");
		// 2. Information is inputted

		// 3. Press on [Send] button
		driver.findElement(By.id("send")).click();
		// 3. Error message is displayed:
		// - "Password must contain at least one number"
		AssertEqualByXpath("//*[@id=\"popup-form-account-create\"]/fieldset/div/div[3]/div[2]/div/span",
						ms.pwNoNumbericError);

		Thread.sleep(3000);

	}


	/**
	 * [[SPEC]]
	 * [[ID]]
	 * 008
	 * [[TITLE]]
	 * Validate error message when registering a new account with re-password does not match with password
	 * [[CHECKPOINT]]
	 * Error message
	 * [[HOWTO]]
	 * 1. Press on [Register] button on the top right
	 * 2. Input register information but re-password does not match with password
	 * 3. Press on [Send] button
	 * [[SUCCESS]]
	 * 1. Register menu is opened
	 * 2. Information is inputted
	 * 3. Error message is displayed:
	 * - "Passwords did not match"
	 * [[ENVIRONMENT]]
	 * 1. Access the URL www.lazada.vn
	 * [[NOTE]]
	 * 
	 * [[PRIORITY]]
	 * 
	 * [[TAG]]
	 * 
	 * [[FILE]]
	 * 
	 * [[BUG]]
	 * 
	 * [[ENDSPEC]]
	 **/
	@Test(enabled = true, groups = { "CI" })
	public void Testcase_008() throws Exception  {

		// [ENVIRONMENT]
		// 1. Access the URL www.lazada.vn

		// [HOWTO]
		// 1. Press on [Register] button on the top right
		OpenSignUpMenu();
		// 1. Register menu is opened

		// 2. Input register information but re-password does not match with password
		driver.findElement(By.id("RegistrationForm_first_name")).sendKeys("My name");
		driver.findElement(By.id("RegistrationForm_email")).sendKeys("myemail@gmail.com");
		driver.findElement(By.id("RegistrationForm_password")).sendKeys("abc123");
		driver.findElement(By.id("RegistrationForm_password2")).sendKeys("notmatch");
		// 2. Information is inputted

		// 3. Press on [Send] button
		driver.findElement(By.id("send")).click();
		// 3. Error message is displayed:
		// - "Passwords did not match"
		AssertEqualByXpath("//*[@id=\"popup-form-account-create\"]/fieldset/div/div[3]/div[2]/div/span",
						ms.pwNotMatchError);

		Thread.sleep(3000);

	}

	/**
	 * Open sign up menu
	 * @throws Exception
	 */
	private void OpenSignUpMenu() throws Exception {
		if (!isElementPresent(By.id("send"))) {
			driver.findElement(By.xpath("/html/body/div[1]/div[1]/div/ul/li[5]/span")).click();
			waiting.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.id("send")));
		}
		// Clear all field before test
		driver.findElement(By.id("RegistrationForm_first_name")).clear();
		driver.findElement(By.id("RegistrationForm_email")).clear();
		driver.findElement(By.id("RegistrationForm_password")).clear();
		driver.findElement(By.id("RegistrationForm_password2")).clear();
	}

}
