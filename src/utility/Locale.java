package utility;

/*
 * Define language message checking.
 */
public class Locale {
	// Login error message
	public String userEmptyError;
	public String emailEmptyError;
	public String emailInvalidError;
	public String pwEmptyError;
	public String pwShortError;
	public String pwNoAlphabeError;
	public String pwNoNumbericError;
	public String pwNotMatchError;
}
