package utility;

public class LocaleVI extends Locale {
	// Language is Vietnamese
	public LocaleVI() {
		// Login error message
		userEmptyError = "Thông tin bắt buộc";
		emailEmptyError = "Thông tin bắt buộc";
		emailInvalidError = "Mail không hợp lệ";
		pwEmptyError = "Mật khẩu phải có ít nhất 1 chữ số";
		pwShortError = "Mật khẩu phải có ít nhất 6 kí tự";
		pwNoAlphabeError = "Mật khẩu phải có ít nhất 01 chữ số (hoặc kí tự chữ)";
		pwNoNumbericError = "Mật khẩu phải có ít nhất 1 chữ số";
		pwNotMatchError = "Mật khẩu không trùng khớp";
	}
}
