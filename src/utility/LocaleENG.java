package utility;

public class LocaleENG extends Locale {
	// Language is English

	public LocaleENG() {
		// Login error message
		userEmptyError = "Required field";
		emailEmptyError = "Required field";
		emailInvalidError = "Invalid mail";
		pwEmptyError = "Password must contain at least one number";
		pwShortError = "Password is too short (minimum is 6 characters).";
		pwNoAlphabeError = "Password must contain at least one alphanumeric";
		pwNoNumbericError = "Password must contain at least one number";
		pwNotMatchError = "Passwords did not match";
	}
}
