package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;

public class ExcelUtils { 
	private static int c_id = 0;
	private static int c_title = 1;
	private static int c_checkpoint = 2;
	private static int c_howto = 3;
	private static int c_success = 4;
	private static int c_environment = 5;
	private static int c_note = 6;
	private static int c_priority = 7;
	private static int c_tag = 8;
	private static int c_file0 = 9;
	private static int c_result = 10;
	private static int c_tester = 11;
	private static int c_date = 12;
	private static int c_version = 13;
	private static int c_bug = 14;
	private static int c_report = 15;
	private static int c_effort = 16;
	private static int c_file = 17;
	private static int c_target = 18;

	private static HSSFWorkbook workbook;
	private static HSSFSheet sheet;
	private static Row row;
	private String fileName = "";
	private String sheetName = "";
	
	/**
	 * Constructor
	 */
	public ExcelUtils() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Constructor
	 * @param fileName
	 * @param sheetName
	 * @throws Exception
	 */
	public ExcelUtils(String fileName, String sheetName) throws Exception {
		// TODO Auto-generated constructor stub
		this.fileName = fileName + ".xls";
		this.sheetName = sheetName;
	}
	
	
	/**
	 * Set sheet name
	 * @param sheetName
	 */
	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	
	/**
	 * Get filename
	 * @return
	 */
	public String getFilename() {
		return fileName;
	}

	/**
	 * Create a test result file
	 * @throws Exception
	 */
	public void createTestResultFile() throws Exception {
		try {
			workbook = new HSSFWorkbook();
			sheet = workbook.createSheet(sheetName);

			// Set cell style
			CellStyle style;

			row = sheet.createRow((short) 0);
			row.createCell(c_id).setCellValue("ID");
			row.createCell(c_title).setCellValue("TITLE");
			row.createCell(c_checkpoint).setCellValue("CHECKPOINT");
			row.createCell(c_howto).setCellValue("HOWTO");
			row.createCell(c_success).setCellValue("SUCCESS");
			row.createCell(c_environment).setCellValue("ENVIRONMENT");
			row.createCell(c_note).setCellValue("NOTE");
			row.createCell(c_priority).setCellValue("PRIORITY");
			row.createCell(c_tag).setCellValue("TAG");
			row.createCell(c_file0).setCellValue("FILE");
			row.createCell(c_result).setCellValue("RESULT");
			row.createCell(c_tester).setCellValue("TESTER");
			row.createCell(c_date).setCellValue("DATE");
			row.createCell(c_version).setCellValue("VERSION");
			row.createCell(c_bug).setCellValue("BUG");
			row.createCell(c_report).setCellValue("REPORT");
			row.createCell(c_effort).setCellValue("EFFORT");
			row.createCell(c_file).setCellValue("FILE");
			row.createCell(c_target).setCellValue("TARGET");

			// Set cell style
			style = createCellStyle(HSSFColor.LIGHT_ORANGE.index);
			row.getCell(c_id).setCellStyle(style);

			style = createCellStyle(HSSFColor.LIGHT_GREEN.index);
			for (int i = c_title; i <= c_file0; i++) {
				row.getCell(i).setCellStyle(style);
			}

			style = createCellStyle(HSSFColor.ROYAL_BLUE.index);
			for (int i = c_result; i <= c_target; i++) {
				row.getCell(i).setCellStyle(style);
			}

			FileOutputStream fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
		} catch (Exception e) {
			throw (e);
		}
	}
	
	/**
	 * Create excel result temp file, without header row
	 * @throws Exception
	 */
	public void createTestResultTempFile() throws Exception {
		try {
			workbook = new HSSFWorkbook();
			sheet = workbook.createSheet(sheetName);

			FileOutputStream fileOut = new FileOutputStream(fileName);
			workbook.write(fileOut);
			fileOut.close();
		} catch (Exception e) {
			throw (e);
		}
	}

	/**
	 * Write test result data from a list to excel file
	 * @param rowIndex
	 * @param classname (class name in test suite)
	 * @param resultList
	 * @throws Exception
	 */
	public void writeTestResultToFile(int rowIndex, String classname, 
			List<TestResult> resultList) throws Exception {
		// Create temp file if not exist
		File f = new File(fileName);
		if (!f.exists())
			createTestResultTempFile();
		
		workbook = new HSSFWorkbook(new FileInputStream(fileName));
		sheet = workbook.getSheetAt(0);

		// Set cell style
		CellStyle style = createCellStyle(HSSFColor.WHITE.index);
		
		// Set class information
		classname = "TETRA" + classname.replace("testSuite", "").replace(".", "/");
		row = sheet.createRow((short) rowIndex++);
		row.createCell(c_id).setCellValue(classname);
		
		// Set test result information
		Iterator<TestResult> iterator = resultList.iterator();
		while (iterator.hasNext()) {
			TestResult result = iterator.next();
			row = sheet.createRow((short) rowIndex++);
			row.createCell(c_id).setCellValue(result.getId());
			row.createCell(c_result).setCellValue(result.getResult());
			row.createCell(c_tester).setCellValue("Selenium");
			row.createCell(c_date).setCellValue(
					new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			row.createCell(c_version).setCellValue(result.getVersion());
			// replace some word in the report string for TETRA by syamamoto 2015-05-28
			row.createCell(c_report).setCellValue(result.getReport().replace("\\\"", "\\”").replace("'", "’") );
			
			// Set cell format
			for (int i = c_id; i <= c_target; i++) {
				if (row.getCell(i, Row.RETURN_BLANK_AS_NULL) == null) {
					row.createCell(i);
				}
				row.getCell(i).setCellStyle(style);
			}
		}
		// Write to file
		FileOutputStream fileOut = new FileOutputStream(fileName);
		workbook.write(fileOut);
		fileOut.close();
	}
	
	/**
	 * Create style for test result file with default workbook
	 * @param colorIndex
	 * @return
	 */
	private CellStyle createCellStyle(short colorIndex) {
		CellStyle style = workbook.createCellStyle();
		// Set background color
		style.setFillForegroundColor(colorIndex);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

		// Set border
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		
		// Set wrap
		style.setWrapText(true);
		
		// Set alignment
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_TOP);
		
		return style;
	}
	
	/**
	 * Merge all sheets in excels file into one
	 * @param Output file
	 * @param List of input file
	 * @throws IOException
	 */
	public void mergeExcelFiles(File file, List<FileInputStream> list)
			throws IOException {
		HSSFWorkbook book = new HSSFWorkbook();
		HSSFSheet sheet = book.createSheet(file.getName());

		for (FileInputStream fin : list) {
			HSSFWorkbook b = new HSSFWorkbook(fin);
			for (int i = 0; i < b.getNumberOfSheets(); i++) {
				copySheets(book, sheet, b.getSheetAt(i));
			}
		}

		try {
			writeFile(book, file);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Write to a file
	 * @param Excel file
	 * @param File path
	 * @throws Exception
	 */
	protected void writeFile(HSSFWorkbook book, File file)
			throws Exception {
		FileOutputStream out = new FileOutputStream(file);
		// Remove first empty row
		HSSFSheet sheet = book.getSheetAt(0);
		
		// Remove first blank row
		int lastRowNum = sheet.getLastRowNum();
		sheet.shiftRows(1, lastRowNum, -1);
		
		book.write(out);
		out.close();
	}

	/**
	 * Copy a sheet in excel file
	 * @param Excel file which will be copied
	 * @param Excel sheet name which will be copied
	 * @param sheet
	 */
	private void copySheets(HSSFWorkbook newWorkbook,
			HSSFSheet newSheet, HSSFSheet sheet) {
		copySheets(newWorkbook, newSheet, sheet, true);
	}

	/**
	 * Copy sheets in excel file
	 * @param newWorkbook
	 * @param newSheet
	 * @param sheet
	 * @param copyStyle
	 */
	private void copySheets(HSSFWorkbook newWorkbook,
			HSSFSheet newSheet, HSSFSheet sheet, boolean copyStyle) {
		int newRownumber = newSheet.getLastRowNum() + 1;
		int maxColumnNum = 0;
		Map<Integer, HSSFCellStyle> styleMap = (copyStyle) ? new HashMap<Integer, HSSFCellStyle>()
				: null;

		for (int i = sheet.getFirstRowNum(); i <= sheet.getLastRowNum(); i++) {
			HSSFRow srcRow = sheet.getRow(i);
			HSSFRow destRow = newSheet.createRow(i + newRownumber);
			if (srcRow != null) {
				copyRow(newWorkbook, sheet, newSheet, srcRow, destRow, styleMap);
				if (srcRow.getLastCellNum() > maxColumnNum) {
					maxColumnNum = srcRow.getLastCellNum();
				}
			}
		}
		for (int i = 0; i <= maxColumnNum; i++) {
			newSheet.setColumnWidth(i, sheet.getColumnWidth(i));
		}
	}

	/**
	 * Copy rows
	 * @param newWorkbook
	 * @param srcSheet
	 * @param destSheet
	 * @param srcRow
	 * @param destRow
	 * @param styleMap
	 */
	private void copyRow(HSSFWorkbook newWorkbook, HSSFSheet srcSheet,
			HSSFSheet destSheet, HSSFRow srcRow, HSSFRow destRow,
			Map<Integer, HSSFCellStyle> styleMap) {
		destRow.setHeight(srcRow.getHeight());
		for (int j = srcRow.getFirstCellNum(); j <= srcRow.getLastCellNum(); j++) {
			HSSFCell oldCell = srcRow.getCell(j);
			HSSFCell newCell = destRow.getCell(j);
			if (oldCell != null) {
				if (newCell == null) {
					newCell = destRow.createCell(j);
				}
				copyCell(newWorkbook, oldCell, newCell, styleMap);
			}
		}
	}

	/**
	 * Copy cells
	 * @param newWorkbook
	 * @param oldCell
	 * @param newCell
	 * @param styleMap
	 */
	private void copyCell(HSSFWorkbook newWorkbook, HSSFCell oldCell,
			HSSFCell newCell, Map<Integer, HSSFCellStyle> styleMap) {
		if (styleMap != null) {
			int stHashCode = oldCell.getCellStyle().hashCode();
			HSSFCellStyle newCellStyle = styleMap.get(stHashCode);
			if (newCellStyle == null) {
				newCellStyle = newWorkbook.createCellStyle();
				newCellStyle.cloneStyleFrom(oldCell.getCellStyle());
				styleMap.put(stHashCode, newCellStyle);
			}
			newCell.setCellStyle(newCellStyle);
		}
		switch (oldCell.getCellType()) {
		case HSSFCell.CELL_TYPE_STRING:
			newCell.setCellValue(oldCell.getRichStringCellValue());
			break;
		case HSSFCell.CELL_TYPE_NUMERIC:
			newCell.setCellValue(oldCell.getNumericCellValue());
			break;
		case HSSFCell.CELL_TYPE_BLANK:
			newCell.setCellType(HSSFCell.CELL_TYPE_BLANK);
			break;
		case HSSFCell.CELL_TYPE_BOOLEAN:
			newCell.setCellValue(oldCell.getBooleanCellValue());
			break;
		case HSSFCell.CELL_TYPE_ERROR:
			newCell.setCellErrorValue(oldCell.getErrorCellValue());
			break;
		case HSSFCell.CELL_TYPE_FORMULA:
			newCell.setCellFormula(oldCell.getCellFormula());
			break;
		default:
			break;
		}
	}
	
}
