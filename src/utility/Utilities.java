package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Utilities { 
	/**
	 * Convert throwable message to string
	 * 
	 * @param throwable
	 * @return
	 */
	public static String getStackTrace(Throwable ex) { 
		if (ex == null) {
			return "";
		}
		StringWriter str = new StringWriter();
		PrintWriter writer = new PrintWriter(str);
		try {
			ex.printStackTrace(writer);
			return str.getBuffer().toString();
		} finally {
			try {
				str.close();
				writer.close();
			} catch (IOException e) {
				// ignore
			}
		}
	}
	
	/**
	 * Remove all files in a directory include the dir
	 * @param path
	 */
	public static void removeDir(String path) {
		File dir = new File(path);
		if (dir.exists()) {
			File[] files = dir.listFiles();
			for (File file : files)
				file.delete();
			dir.delete();
		}
	}
	
	/**
	 * Make a directory 
	 * @param path
	 */
	public static void makeDir(String path) {
		File dir = new File(path);
		dir.mkdir();
	}
	
	/**
	 * Get all file names in a directory to list 
	 * @param path
	 * @return
	 * @throws IOException 
	 */
	public static List<FileInputStream> getFilesInDir(String path) throws IOException {
		List<FileInputStream> listFile = new ArrayList<FileInputStream>();
		File dir = new File(path);
		File[] files = dir.listFiles();
		Arrays.sort(files);
		for (File file : files) {
			if (file.isFile()) {
				listFile.add(new FileInputStream(file.getAbsolutePath()));
			}
		}
		return listFile;
	}
	
	/**
	 * Get timestamp
	 * @return
	 */
	public static String timestamp() {
		String timestamp = new SimpleDateFormat("yyyyMMddhhmmss")
			.format(new Date());
		return timestamp;
	}

}
