package utility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class ParseUtils extends ArrayList {
	private String txtFilename;

	/**
	 * Constructor
	 * @param txtFilename
	 * @throws IOException
	 */
	public ParseUtils() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Constructor
	 * @param txtFilename
	 * @throws IOException
	 */
	public ParseUtils(String txtFilename) throws IOException {
		this.txtFilename = txtFilename;
	}
	
	
	/**
	 * Get error
	 * @param className
	 * @param tr
	 * @return
	 */
	public String traceError(String className, Throwable tr) {
		String error = "";
		String classPath = "./src/" + className.replace(".", "/") + ".java";
		className =  className.substring(className.lastIndexOf('.') + 1);
		String pattern = "\\(" + className + "(?:(?!\\)).)+?\\)";
		
		// Get Matcher
		Matcher mInput = Pattern.compile(pattern, Pattern.DOTALL).matcher(Utilities.getStackTrace(tr));
		while (mInput.find()) {
			String s = mInput.group();
			s =  s.substring(s.lastIndexOf(":") + 1);
			s = s.replace(")", "");	
			try {
				error = traceErrorLine(classPath, Integer.parseInt(s));
//				if (tr.getMessage() != null && !tr.getLocalizedMessage().isEmpty())
//					error += "\r\n" + tr.getLocalizedMessage();
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return error;
	}

	
	private String traceErrorLine(String fileName, int lineNum) {
		try {
			StringBuffer sb = new StringBuffer();
			BufferedReader in = new BufferedReader(new FileReader(fileName));
			String pattern = "^\\/\\/(?s).*";
			String s;
			int i = 1;
			while ((s = in.readLine()) != null) {
				Matcher mInput = Pattern.compile(pattern, Pattern.DOTALL).matcher(s);
				if (s.contains("\t//")) {
					sb = new StringBuffer();
					sb.append(s);
					sb.append("\n");
				}
				if (i == lineNum) {
					sb.append(s);
					sb.append("\n");
					break;
				}
				i++;			
			}
			in.close();
			return sb.toString();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return "";
		}
	}
}
