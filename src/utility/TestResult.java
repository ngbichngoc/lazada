package utility;

public class TestResult {
	private String id;
	private String result;
	private String version;
	private String report;
	
	public TestResult(String id, String result, String version, String report) {  
		// TODO Auto-generated constructor stub
		this.id = id;
		this.result = result;
		this.version = version;
		this.report = report;
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getResult() {
		return result;
	}
	
	public void setResult(String result) {
		this.result = result;
	}
	
	public String getReport() {
		return report;
	}
	
	public String getVersion() {
		return version;
	}
}
